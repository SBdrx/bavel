package bavel.members;


import bavel.Controller;
import bavel.model.Member;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Import implements AutoCloseable {
    //region FXML Fields
    @FXML private TableView<ImportRow> tablePreview;
    @FXML private ChoiceBox<ImportCol> choicePreName;
    @FXML private ChoiceBox<ImportCol> choiceName;
    @FXML private ChoiceBox<String> choiceCharset;
    @FXML private TextField txtSeparator;
    @FXML private TextField txtTextChar;
    @FXML private CheckBox cbTextEscape;
    @FXML private Button btnOkay;
    @FXML private Button btnAbort;
    @FXML private Label lblFile;
    @FXML private Button btnOpen;
    //endregion

    private final ObjectProperty<File> importFile = new SimpleObjectProperty<>(null);
    private final ObservableList<ImportRow> rows = FXCollections.observableArrayList();
    private final ObservableList<ImportCol> cols = FXCollections.observableArrayList();
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final ObjectProperty<Callback<List<Member>, Boolean>> callbackObjectProperty = new SimpleObjectProperty<>();
    private final ObservableList<String> charsets = FXCollections.observableArrayList();
    //region Handler
    @FXML
    void onBtnOpen(ActionEvent event) {
        showOpenFileDialog();
    }

    @FXML
    void onBtnAbort(ActionEvent event) {
        ((Stage)btnAbort.getScene().getWindow()).close();
    }

    @FXML
    void onBtnOkay(ActionEvent event) {
        ArrayList<Member> members = new ArrayList<>();
        for (ImportRow row : rows) {
            members.add(new Member(
                    row.getValue(choiceName.getValue()),
                    row.getValue(choicePreName.getValue())
            ));
        }
        if (callbackObjectProperty.get().call(members)) {
            onBtnAbort(event);
        }
    }
    //endregion

    @FXML
    void initialize() {
        assert tablePreview != null : "fx:id=\"tablePreview\" was not injected: check your FXML file 'import.fxml'.";
        assert choicePreName != null : "fx:id=\"choicePreName\" was not injected: check your FXML file 'import.fxml'.";
        assert txtSeparator != null : "fx:id=\"txtSeparator\" was not injected: check your FXML file 'import.fxml'.";
        assert txtTextChar != null : "fx:id=\"txtTextChar\" was not injected: check your FXML file 'import.fxml'.";
        assert choiceName != null : "fx:id=\"choiceName\" was not injected: check your FXML file 'import.fxml'.";
        assert cbTextEscape != null : "fx:id=\"cbTextEscape\" was not injected: check your FXML file 'import.fxml'.";
        assert btnOkay != null : "fx:id=\"btnOkay\" was not injected: check your FXML file 'import.fxml'.";
        assert btnAbort != null : "fx:id=\"btnAbort\" was not injected: check your FXML file 'import.fxml'.";
        assert lblFile != null : "fx:id=\"lblFile\" was not injected: check your FXML file 'import.fxml'.";
        assert btnOpen != null : "fx:id=\"btnOpen\" was not injected: check your FXML file 'import.fxml'.";

        importFile.addListener((observable, oldValue, newValue) -> {
            lblFile.setText(newValue.getAbsolutePath());
            reloadFile();
        });

        txtTextChar.textProperty().addListener((observable, oldValue, newValue) -> reloadFile());
        txtSeparator.textProperty().addListener((observable, oldValue, newValue) -> reloadFile());
        cbTextEscape.selectedProperty().addListener((observable, oldValue, newValue) -> reloadFile());
        choiceCharset.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> reloadFile());

        choiceName.setItems(cols);
        choicePreName.setItems(cols);
        StringConverter<ImportCol> converter = new StringConverter<ImportCol>() {
            @Override
            public String toString(ImportCol object) {
                return object.toString();
            }

            @Override
            public ImportCol fromString(String string) {
                for (ImportCol col : cols) {
                    if (col.toString().equalsIgnoreCase(string)) return col;
                }
                return null;
            }
        };
        choiceName.setConverter(converter);
        choicePreName.setConverter(converter);
       /* BooleanBinding colsSelected = new BooleanBinding() {
            @Override
            protected boolean computeValue() {
                return choiceName.getValue()!=null && choicePreName.getValue()!=null && cols.size()>0;
            }
        };*/

       charsets.addAll(Charset.availableCharsets().keySet());
       choiceCharset.setItems(charsets);
       if (charsets.size()>0) {
           choiceCharset.getSelectionModel().select(Charset.defaultCharset().name());
       }

        btnOkay.disableProperty().bind(
                callbackObjectProperty.isNull().or(importFile.isNull()) // .or(colsSelected.not())
        );

    }

    public void showOpenFileDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Importieren von Mitgliederdaten");
        fileChooser.setInitialDirectory(Controller.getLastDirectory());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV-Dateien", "*.csv"),
                new FileChooser.ExtensionFilter("Excel Dateien", "*.xls")
        );
        fileChooser.selectedExtensionFilterProperty().setValue(fileChooser.getExtensionFilters().get(0));
        File file = fileChooser.showOpenDialog(btnOpen.getScene().getWindow());
        if (file!=null && file.isFile()) {
            Controller.saveLastDirectory(file);
            importFile.setValue(file);
        }
    }

    private void reloadFile() {
        if (importFile.get()==null) return;
        // Remove Current
        Platform.runLater(() -> {
            choicePreName.getSelectionModel().select(null);
            choiceName.getSelectionModel().select(null);
            this.rows.clear();
            this.cols.clear();
            tablePreview.getColumns().clear();
        });

        executor.execute(() -> {

            try (FileInputStream inputStream = new FileInputStream(importFile.get())) {
                boolean headers = false;
                final ArrayList<ImportCol> cols = new ArrayList<>();
                final ArrayList<ImportRow> rows = new ArrayList<>();
                List<String> lines;
                while ( (lines = readLine(inputStream)) != null) {
                    if (headers) {
                        // To ROW:
                        rows.add(new ImportRow(lines));
                    } else {
                        // to Col
                        for (int i = 0; i < lines.size(); i++) {
                            cols.add(new ImportCol(i, lines.get(i)));
                        }
                        headers = true;
                    }
                }

                // And Back to Main Thread
                Platform.runLater(() -> {
                    this.rows.clear();
                    this.cols.clear();
                    tablePreview.getColumns().clear();
                    for (ImportCol col : cols) {
                        ImportTableColumn e = new ImportTableColumn(col);
                        e.setText(col.name);
                        tablePreview.getColumns().add(e);
                    }
                    this.rows.addAll(rows);
                    this.cols.addAll(cols);
                    tablePreview.setItems(this.rows);
                    for (ImportCol col : cols) {
                        if (col.name.equalsIgnoreCase("vorname")) {
                            choicePreName.getSelectionModel().select(col);
                        } else if (col.name.toLowerCase().endsWith("name") &&
                                (
                                        choiceName.getSelectionModel().getSelectedItem()==null
                                        || choiceName.getSelectionModel().getSelectedItem().name.length()>col.name.length()
                                )
                        ) {
                            choiceName.getSelectionModel().select(col);
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private ArrayList<String> readLine(FileInputStream inputStream) throws IOException {
        byte sepByte;
        byte[] textByte;
        boolean allowEscape = cbTextEscape.isSelected();
        if (txtSeparator.getText().length()>0) {
            sepByte = txtSeparator.getText().getBytes()[0];
        } else {
            sepByte = (byte)0x3B;
        }
        if (txtTextChar.getText().length()>0) {
            textByte = new byte[]{txtTextChar.getText().getBytes()[0]};
        } else {
            textByte = new byte[0];
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        byte[] c = new byte[1];
        boolean esc = false;
        int inText = 0;
        int p = 0;
        ArrayList<String> cols = new ArrayList<>();
        while ( (inputStream.read(c))!=-1) {
            if (inText==0 && (c[0] == (byte)0x0A || c[0] == (byte)0x0D)) {
                if (p>0) {
                    if (buffer.size()>0) {
                        cols.add(new String(buffer.toByteArray(), importCharset()));
                        buffer.reset();
                    }
                    break; // we have reached the end of Line.
                } // else we will ignore it - it is on the beginning
            } else {
                if (textByte.length>0 && c[0] == textByte[0]) {
                    // Es ist das Text Zeichen
                    if (buffer.size()==0) {
                        // Ein Feld startet!
                        inText = 1;
                    } else if (allowEscape && esc) {
                        buffer.write(c);
                        esc = false;
                    } else if (allowEscape) {
                        esc = true;
                    } else {
                        inText = 2;
                    }
                } else if ((esc && c[0]==sepByte) || (inText==0 && c[0]==sepByte)) {
                    inText = 0;
                    esc = false;
                    cols.add(new String(buffer.toByteArray(),importCharset()));
                    buffer.reset();
                } else if (esc){
                    // we shout not end here !
                    throw new IOException("Non Expected Charetar after a Text-Char- allowed are Seperator or Text-Char");
                } else {
                    buffer.write(c);
                }
            }



            p++;
        }
        if (cols.size()==0) return null;
        return cols;
    }

    private Charset importCharset() {
        return Charset.forName(choiceCharset.getValue()!=null&&!choiceCharset.getValue().isEmpty()?choiceCharset.getValue():Charset.defaultCharset().name());
    }

    public Callback<List<Member>, Boolean> getCallbackObjectProperty() {
        return callbackObjectProperty.get();
    }

    public ObjectProperty<Callback<List<Member>, Boolean>> callbackObjectPropertyProperty() {
        return callbackObjectProperty;
    }

    public void setCallbackObjectProperty(Callback<List<Member>, Boolean> callbackObjectProperty) {
        this.callbackObjectProperty.set(callbackObjectProperty);
    }

    @Override
    public void close() {
        executor.shutdownNow();
    }
}