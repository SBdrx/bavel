package bavel.members;

import java.util.Objects;

public class ImportCol {
    public final String name;
    public final int idx;

    public ImportCol(int idx, String name) {
        this.idx = idx;
        this.name = name!=null?name.trim():"";
    }

    public String getName() {
        return name;
    }

    public int getIdx() {
        return idx;
    }

    @Override
    public String toString() {
        return ((idx + 1 ) + ". " + name).trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportCol importCol = (ImportCol) o;
        return idx == importCol.idx &&
                Objects.equals(name, importCol.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, idx);
    }
}
