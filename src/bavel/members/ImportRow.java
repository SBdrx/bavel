package bavel.members;

import java.util.ArrayList;
import java.util.List;

public class ImportRow {
    public final ArrayList<String> cols = new ArrayList<>();
    public ImportRow(List<String> lines) {
        cols.addAll(lines);
    }

    public String getValue(ImportCol col) {
        if (col.getIdx()>=0 && col.getIdx()<cols.size()) {
            return cols.get(col.getIdx());
        }
        return "";
    }
}
