package bavel.members;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;

public class ImportTableColumn extends TableColumn<ImportRow, String> {
    private final ImportCol col;
    public ImportTableColumn(ImportCol col) {
        super();
        this.col = col;
        this.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue(col)));
    }


}
