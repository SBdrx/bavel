package bavel;

import bavel.display.DisplayPane;
import bavel.display.DisplayPaneSettings;
import bavel.display.DrawEvent;
import bavel.display.WriteJpegHandler;
import bavel.members.Import;
import bavel.model.CountMember;
import bavel.model.Member;
import bavel.model.Store;
import bavel.model.Topic;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Controller {

    //region FXML Fields
    @FXML private ResourceBundle resources;
    @FXML private URL location;
    @FXML private Tab tabText;
    @FXML private Tab tabCount;
    @FXML private TextArea htmlTextEdit;
    @FXML private Button btnTextApply;
    @FXML private Tab tabWahl;
    @FXML private TableView<Member> tableWahl;
    @FXML private TableColumn<Member, String> colName;
    @FXML private TableColumn<Member, String> colPrename;
    @FXML private Button btnWahlAdd;
    @FXML private Button btnWahlRemove;
    @FXML private TextField txtWahlIn;
    @FXML private Button btnWahlCustomAdd;
    @FXML private Label lblPunkt;
    @FXML private ListView<Topic> listItems;
    @FXML private ScrollPane scrollLeft;
    @FXML private Pane previewPane;
    @FXML private Button btnAdd;
    @FXML private Button btnDel;
    @FXML private Button btnNext;
    @FXML private ToggleButton btnTglRecord;
    @FXML private MenuItem menuNew;
    @FXML private MenuItem menuLoad;
    @FXML private MenuItem menuSave;
    @FXML private MenuItem menuClose;
    @FXML private TextField txtTopicThema;
    @FXML private ListView<CountMember> listWahlSelected;
    @FXML private TabPane topicTabs;
    @FXML private Button btnCountReset;
    @FXML private Button btnCountNone;
    @FXML private Button btnCountAll;
    @FXML private Button btnCountNext;
    @FXML private Label lblCount;
    @FXML private ListView<CountMember> listCount;
    @FXML private CheckBox cbWahlBlock;
    //endregion

    private DisplayPane displayPane;

    //region Topics
    private final ObservableList<Topic> topics = FXCollections.observableArrayList();
    private final ObjectProperty<Topic> selectedTopic = new SimpleObjectProperty<>(null);
    private static File openSaveDirectory = new File(System.getProperty("user.home"));

    private void initTopics() {
        topics.addListener((ListChangeListener<? super Topic>) c -> {
            // Loop Through Topics and set NUMBER
            synchronized (topics) {
                int top = 1;
                for (Topic topic : topics) {
                    topic.setTop(top++);
                }
            }
        });

        listItems.setCellFactory(param -> new ListCell<Topic>(){
            @Override
            protected void updateItem(Topic item, boolean empty) {
                super.updateItem(item, empty);
                textProperty().unbind();
                if (empty || item==null) {
                    setText("");
                } else {
                    textProperty().bind(item.topProperty().asString("%d ").concat(item.themaProperty()));
                }
            }
        });
        listItems.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        listItems.getSelectionModel().getSelectedIndices().addListener((ListChangeListener<? super Integer>) c -> {
            selectedTopic.setValue(listItems.getSelectionModel().getSelectedItem());
        });

        selectedTopic.addListener((observable, oldValue, newValue) -> {
            if (oldValue!=null) {
                txtTopicThema.textProperty().unbindBidirectional(oldValue.themaProperty());
                lblPunkt.textProperty().unbind();
                htmlTextEdit.textProperty().unbindBidirectional(oldValue.contentProperty());
                if (tabWahl.isSelected()) {
                    oldValue.setMode(1);
                } else if (tabText.isSelected()) {
                    oldValue.setMode(0);
                } else {
                    oldValue.setMode(2);
                }

                listWahlSelected.setItems(selectedMember);
                countBlokWahl.unbindBidirectional(oldValue.blockWahlProperty());
                countMembers.clear();
                counts.unbindBidirectional(oldValue.countsProperty());
            }
            txtTopicThema.setText("");
            lblPunkt.setText("");
            if (newValue!=null) {
                txtTopicThema.textProperty().bindBidirectional(newValue.themaProperty());
                lblPunkt.textProperty().bind(newValue.topProperty().asString());
                htmlTextEdit.textProperty().bindBidirectional(newValue.contentProperty());
                listWahlSelected.setItems(newValue.getMembers());
                countBlokWahl.bindBidirectional(newValue.blockWahlProperty());
                counts.bindBidirectional(newValue.countsProperty());
                if (newValue.getMode()==2) {
                    topicTabs.getSelectionModel().select(tabCount);
                } else if (newValue.getMode()==1) {
                    topicTabs.getSelectionModel().select(tabWahl);
                } else {
                    topicTabs.getSelectionModel().select(tabText);
                }
            }
        });

        listItems.setItems(topics);
        this.displayPane.topicProperty().bind(selectedTopic);
    }

    @FXML
    void onBtnAddClick(ActionEvent event) {
        // event can be null!!
        addNewTopic();
    }

    @FXML
    void onBtnDelClick(ActionEvent event) {
        if(listItems.getItems().size()>1 && !listItems.getSelectionModel().isEmpty()) {
            listItems.getItems().removeAll(listItems.getSelectionModel().getSelectedItems());
        }
        btnDel.setDisable(listItems.getItems().size()<=1);
    }

    private void addNewTopic() {
        Topic newTopic = new Topic();
        newTopic.setThema("Neues Topic" + (topics.size()>0?" " + topics.size():""));
        synchronized (topics) {
            topics.add(newTopic);
        }
        listItems.getSelectionModel().select(newTopic);
        txtTopicThema.selectAll();
        txtTopicThema.requestFocus();
        btnDel.setDisable(listItems.getItems().size()<=1);
       /* // Debug:
        newTopic.contentProperty().setValue("Dies ist eine\nInfo zum Topic\ndie gut und cool dargestellt\nwerden soll!");
        for (int i = 0; i < 10; i++) {
            newTopic.getMembers().add(new CountMember(new Member("Demo " + (i+1), "Vorname")));
        }*/
    }

    @FXML
    void onBtnNextClick(ActionEvent event) {
        int idx = listItems.getSelectionModel().getSelectedIndex();
        if (idx + 1 < listItems.getItems().size()) {
            listItems.getSelectionModel().select(idx+1);
        }
    }
    //endregion

    //region Aufnahme
    private Stage recordStage= null;
    DisplayPaneSettings recordWindowSettings= null;
    DisplayPane recoredPane = null;

    @FXML
    void onBtnTglRecordClicck(ActionEvent event) {
        if (recordStage==null) {
            openRecordWindow();
        } else {
            closeRecordWindow();
        }
    }

    private void closeRecordWindow() {
        recoredPane.topicProperty().unbind();
        recordStage.close();
        recordStage = null;
        recordWindowSettings = null;
        DrawEvent.Handler handler = recoredPane.getDrawEventEventHandler();
        if (handler instanceof AutoCloseable) {
            try {
                ((AutoCloseable) handler).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        recoredPane = null;

    }

    private void openRecordWindow() {
        if (Screen.getScreens().size()>1) {
            Screen possible = null;
            Point2D point = new Point2D(
                    txtTopicThema.getScene().getWindow().getX() + txtTopicThema.getScene().getWindow().getWidth()/2,
                    txtTopicThema.getScene().getWindow().getY() + txtTopicThema.getScene().getWindow().getHeight()/2);
            for (Screen screen : Screen.getScreens()) {

                if (!screen.getBounds().contains(point)) {
                    System.out.println(screen.getBounds().toString() + " does not contain " + point.toString());
                    possible = screen;
                } else {
                    System.out.println(screen.getBounds().toString() + " contains " + point.toString());
                }
            }
            if (possible!=null) {
                WriteJpegHandler writeJpegHandler = new WriteJpegHandler(new File("./", "record_" + System.currentTimeMillis()));
                recordStage = new Stage();
                recordStage.initStyle(StageStyle.UNDECORATED);
                recordStage.setAlwaysOnTop(true);
                recordWindowSettings = new DisplayPaneSettings(possible.getBounds().getWidth(), possible.getBounds().getHeight());
                Pane pane = new Pane();
                pane.setStyle("-fx-background-color: #" + (recordWindowSettings.mainColor().toString().substring(2)));
                recordStage.setMaxHeight(recordWindowSettings.getHeight());
                recordStage.setMinHeight(recordWindowSettings.getHeight());
                recordStage.setMaxWidth(recordWindowSettings.getWidth());
                recordStage.setMinWidth(recordWindowSettings.getWidth());
                recordStage.setScene(new Scene(pane, recordWindowSettings.getWidth(), recordWindowSettings.getHeight()));
                recordStage.show();
                recordStage.setX(possible.getBounds().getMinX());
                recordStage.setY(possible.getBounds().getMinY());
                recoredPane = new DisplayPane(pane, recordWindowSettings);
                recoredPane.setDrawEventEventHandler(writeJpegHandler);
                this.recoredPane.topicProperty().bind(selectedTopic);
                recordStage.getScene().setOnKeyPressed(event1 -> {
                    if (event1.getCode()==KeyCode.ESCAPE) {
                        Platform.runLater(()->{
                            closeRecordWindow();
                            btnTglRecord.setSelected(false);
                        });
                    }
                });
                recordStage.setOnCloseRequest(event -> {
                    closeRecordWindow();
                });
            }
        }
    }
    //endregion

    //region Menu
    @FXML
    void onMenuClose(ActionEvent event) {
        close();
    }

    public void close() {
        if (recoredPane!=null) {
            closeRecordWindow();
        }
        Platform.exit();
    }

    @FXML
    void onMenuLoad(ActionEvent event) {
        FileChooser fileChooser = createFileChooser("Laden einer BaVeL Datei");
        File choosen = fileChooser.showOpenDialog(txtTopicThema.getScene().getWindow());
        if (choosen != null && choosen.isFile()) {
            saveLastDirectory(choosen);
            try (FileInputStream inputStream = new FileInputStream(choosen)){
                Store loaded = Store.deserialize(inputStream);
                topics.clear();
                members.clear();
                countMembers.clear();
                selectedTopic.setValue(null);
                topics.addAll(loaded.getTopics());
                members.addAll(loaded.getMembers());
                if (topics.size()>0) {
                    listItems.getSelectionModel().select(topics.get(0));
                }
                btnDel.setDisable(listItems.getItems().size()<=1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void onMenuNew(ActionEvent event) {
        listItems.getSelectionModel().select(null);
        topics.clear();
        members.clear();
        countMembers.clear();
        addNewTopic();
    }

    private FileChooser createFileChooser(String Title) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(Title);
        fileChooser.setInitialDirectory(getLastDirectory());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("BaVeL-Dateien", "*.bavel"),
                new FileChooser.ExtensionFilter("Alle Dateien", "*.*")
        );
        fileChooser.selectedExtensionFilterProperty().setValue(fileChooser.getExtensionFilters().get(0));
        return fileChooser;
    }

    public static File getLastDirectory() {
        System.out.println("Using last Directory " + openSaveDirectory.getAbsolutePath());
        return openSaveDirectory;
    }

    public static void saveLastDirectory(File file) {
        if (file.isFile()) openSaveDirectory = file.getParentFile();
        else openSaveDirectory = file;
    }


    @FXML
    void onMenuSave(ActionEvent event) {
        FileChooser fileChooser = createFileChooser("Laden einer BaVeL Datei");
        File choosen = fileChooser.showSaveDialog(txtTopicThema.getScene().getWindow());
        if (choosen != null && !choosen.isDirectory()) {
            saveLastDirectory(choosen);
            try (FileOutputStream outputStream = new FileOutputStream(choosen)){
                Store toSave = new Store(topics, members);
                toSave.serializeTo(outputStream);
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @FXML
    void onMenuImport(ActionEvent event) {
        Platform.runLater(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("members/import.fxml"));
                Parent root = loader.load();
                Stage stage = new Stage(StageStyle.DECORATED);
                stage.setTitle("Mitglieder importieren - CSV");
                stage.setScene(new Scene(root));
                stage.show();
                stage.toFront();
                stage.requestFocus();
                InputStream resourceAsStream = Main.class.getResourceAsStream("bavel-b-logo.png");
                if (resourceAsStream!=null) {
                    stage.getIcons().add(new Image(resourceAsStream));
                }
                Platform.runLater(() -> {
                    ((Import)loader.getController()).showOpenFileDialog();
                    ((Import)loader.getController()).setCallbackObjectProperty(param -> {
                        members.clear();
                        members.addAll(param);
                        return true;
                    });
                    stage.setOnCloseRequest(closEvent -> {
                        ((Import)loader.getController()).close();
                    });
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void onMenuAbout(ActionEvent event) {
        openAbout();
    }
    //endregion

    //region WAHL Bereich
    private final ObservableList<Member> members = FXCollections.observableArrayList();
    private final ObservableList<CountMember> selectedMember = FXCollections.emptyObservableList();

    private void initWahl() {
        loadInitialMembers();
        listWahlSelected.setItems(selectedMember);
        listWahlSelected.setCellFactory(param -> new ListCell<CountMember>() {

            @Override
            protected void updateItem(CountMember item, boolean empty) {
                super.updateItem(item, empty);
                if (item==null || empty) {
                    setText("");
                } else {
                    int idx = listWahlSelected.getItems().indexOf(item);
                    setText((idx>=0?idx+1:"?")+ ".\t"  +item.toString());
                }
            }
        });

        FilteredList<Member> filteredData = new FilteredList<>(members, p -> true);

        txtWahlIn.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                return person.getName().toLowerCase().contains(lowerCaseFilter)
                        || person.getPreName().toLowerCase().contains(lowerCaseFilter);
            });
        });

        colName.setCellValueFactory(new PropertyValueFactory<Member, String>("name"));
        colPrename.setCellValueFactory(new PropertyValueFactory<Member, String>("preName"));
        tableWahl.setRowFactory(tableView -> {
            TableRow<Member> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Platform.runLater(this::wahlAddTableEntry);
                }
            });
            return row;
        });


        SortedList<Member> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tableWahl.comparatorProperty());
        tableWahl.setItems(sortedData);



        tabWahl.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (selectedTopic.get()!=null) {
                    selectedTopic.get().setMode(1);
                }
            }
            txtWahlIn.requestFocus();
         });
        tabText.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (selectedTopic.get()!=null) {
                    selectedTopic.get().setMode(0);
                }
            }
            htmlTextEdit.requestFocus();
        });
        tabCount.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (selectedTopic.get()!=null) {
                    if (selectedTopic.get().getMode()!=2) {
                        selectedTopic.get().setMode(2);
                        this.countMembers.clear();
                        this.countMembers.addAll(selectedTopic.get().getMembers());
                        updateDisplays();
                    }
                }
                listCount.requestFocus();
            }
        });


        cbWahlBlock.selectedProperty().bindBidirectional(countBlokWahl);
    }

    private void updateDisplays() {
        Platform.runLater(() -> {
            displayPane.updatePane();
            if (recoredPane!=null) {
                recoredPane.updatePane();
            }
        });
    }

    private void loadInitialMembers() {
        members.add(new Member("Drexler", "Alexander"));
        members.add(new Member("Lingens", "Martin"));
    }


    @FXML
    void btnWahlRemove(ActionEvent event) {
        if (listWahlSelected.getSelectionModel().getSelectedItems().size()>0) {
            listWahlSelected.getItems().removeAll(listWahlSelected.getSelectionModel().getSelectedItems());
        }
        txtWahlIn.setText("");
    }
    @FXML
    void onBtnWahlAdd(ActionEvent event) {
        wahlAddTableEntry();
    }

    private void wahlAddTableEntry() {
        if (tableWahl.getSelectionModel().getSelectedItems().size()>0) {
            for (Member selectedItem : tableWahl.getSelectionModel().getSelectedItems()) {
                if (!listContains(listWahlSelected.getItems(), selectedItem)) {
                    listWahlSelected.getItems().add(new CountMember(selectedItem));
                }
            }
        }
        txtWahlIn.setText("");
    }

    private boolean listContains(List<? extends Member> list, Member member) {
        for (Member test : list) {
            if (
                    member.getPreName().equalsIgnoreCase(test.getPreName())
                    && member.getName().equalsIgnoreCase(test.getName())
            ) {
                return true;
            }
        }
        return false;
    }

    @FXML
    void onBtnWahlCustomAdd(ActionEvent event) {

        String firstName = "";
        String name = txtWahlIn.getText();
        if (name==null || name.trim().isEmpty()) {
            return; // Abort
        }
        if (name.contains(" ")) {
            firstName = name.substring(0, name.indexOf(" ")).trim();
            name = name.substring(firstName.length()).trim();
        }
        Member custom = new Member(name, firstName);
        if (!listContains(listWahlSelected.getItems(), custom)) {
            listWahlSelected.getItems().add(new CountMember(custom));
            txtWahlIn.setText("");
        }
    }

    @FXML
    void onTextWahlAction(ActionEvent event) {
        if (tableWahl.getItems().size()==1) {
            Member m = tableWahl.getItems().get(0);
            if (!listContains(listWahlSelected.getItems(), m)) {
                listWahlSelected.getItems().add(new CountMember(m));
                txtWahlIn.setText("");
                return;
            }
        }
        onBtnWahlCustomAdd(event);
    }


    //endregion

    //region Auszählung
    private final BooleanProperty countBlokWahl = new SimpleBooleanProperty();
    private final IntegerProperty counts = new SimpleIntegerProperty();
    private final ObservableList<CountMember> countMembers = FXCollections.observableArrayList();
    private void initCount() {
        btnCountAll.disableProperty().bind(countBlokWahl.not());
        btnCountNone.disableProperty().bind(countBlokWahl.not());
        btnCountNext.setOnAction(event -> nextVote());
        btnCountNone.setOnAction(event -> countSelectNone());
        btnCountAll.setOnAction(event -> countSelectAll());

        btnCountReset.setOnAction(event -> {
            countSelectNone();
            counts.setValue(0);
            for (CountMember countMember : countMembers) {
                countMember.reset();
            }
            updateDisplays();
        });

        lblCount.textProperty().bind(counts.asString("%d"));
        listCount.setCellFactory(param -> new CountMemberListCell());
        listCount.setOnKeyReleased(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                nextVote();
            } else if (event.getCode().equals(KeyCode.N)) {
                countSelectNone();
            } else if (event.getCode().equals(KeyCode.A)) {
                countSelectAll();
            } else if (event.getCode().isDigitKey()) {
                int number = Integer.parseInt(event.getText());
                if (number>0 && number<=selectedTopic.get().getMembers().size()) {
                    CountMember countMember = selectedTopic.get().getMembers().get(number - 1);
                    countMember.setSelected(!countMember.isSelected());
                }
            }
        });
        listCount.setItems(countMembers);
    }

    private void countSelectNone() {
        selectedTopic.get().getMembers().forEach(member -> member.setSelected(false));
        listCount.requestFocus();
    }

    private void countSelectAll() {
        selectedTopic.get().getMembers().forEach(member -> member.setSelected(true));
        listCount.requestFocus();
    }

    private void nextVote() {
        if (!countBlokWahl.getValue()) {
            AtomicInteger votes = new AtomicInteger();
            selectedTopic.get().getMembers().forEach(member -> {
                if (member.isSelected()) {
                    votes.getAndIncrement();
                }
            });
            if (votes.get()>1) {
                // Show Error!
                btnCountNext.setTextFill(Color.RED);
                return;
            }
        }
        btnCountNext.setTextFill(Color.BLACK);
        AtomicBoolean voted = new AtomicBoolean(false);
        selectedTopic.get().getMembers().forEach(member -> {
            if (member.isSelected()) {
                member.increaseCount();
                voted.set(true);
            }
            member.setSelected(false);
        });
        counts.setValue(counts.getValue() + 1);
        if (!voted.get()) {
            selectedTopic.get().setAbstentions(selectedTopic.get().getAbstentions()+1);
        }
        updateDisplays();
        listCount.requestFocus();
    }
    //endregion

    @FXML
    void initialize() {
        assert tabText != null : "fx:id=\"tabText\" was not injected: check your FXML file 'main.fxml'.";
        assert htmlTextEdit != null : "fx:id=\"htmlTextEdit\" was not injected: check your FXML file 'main.fxml'.";
        assert btnTextApply != null : "fx:id=\"btnTextApply\" was not injected: check your FXML file 'main.fxml'.";
        assert tabWahl != null : "fx:id=\"tabWahl\" was not injected: check your FXML file 'main.fxml'.";
        assert tableWahl != null : "fx:id=\"tableWahl\" was not injected: check your FXML file 'main.fxml'.";
        assert colName != null : "fx:id=\"colName\" was not injected: check your FXML file 'main.fxml'.";
        assert colPrename != null : "fx:id=\"colPrename\" was not injected: check your FXML file 'main.fxml'.";
        assert btnWahlAdd != null : "fx:id=\"btnWahlAdd\" was not injected: check your FXML file 'main.fxml'.";
        assert btnWahlRemove != null : "fx:id=\"btnWahlRemove\" was not injected: check your FXML file 'main.fxml'.";
        assert txtWahlIn != null : "fx:id=\"txtWahlIn\" was not injected: check your FXML file 'main.fxml'.";
        assert btnWahlCustomAdd != null : "fx:id=\"btnWahlCustomAdd\" was not injected: check your FXML file 'main.fxml'.";
        assert lblPunkt != null : "fx:id=\"lblPunkt\" was not injected: check your FXML file 'main.fxml'.";
        assert listItems != null : "fx:id=\"listItems\" was not injected: check your FXML file 'main.fxml'.";
        assert scrollLeft != null : "fx:id=\"scrollLeft\" was not injected: check your FXML file 'main.fxml'.";
        assert previewPane != null : "fx:id=\"previewPane\" was not injected: check your FXML file 'main.fxml'.";
        assert btnAdd != null : "fx:id=\"btnAdd\" was not injected: check your FXML file 'main.fxml'.";
        assert btnNext != null : "fx:id=\"btnNext\" was not injected: check your FXML file 'main.fxml'.";
        assert btnTglRecord != null : "fx:id=\"btnTglRecord\" was not injected: check your FXML file 'main.fxml'.";
        assert menuNew != null : "fx:id=\"menuNew\" was not injected: check your FXML file 'main.fxml'.";
        assert menuLoad != null : "fx:id=\"menuLoad\" was not injected: check your FXML file 'main.fxml'.";
        assert menuSave != null : "fx:id=\"menuSave\" was not injected: check your FXML file 'main.fxml'.";
        assert menuClose != null : "fx:id=\"menuClose\" was not injected: check your FXML file 'main.fxml'.";

        this.displayPane = new DisplayPane(previewPane, new DisplayPaneSettings(800, 600));
        initTopics();
        initWahl();
        initCount();

        // Add a Topic:
        addNewTopic();

        openAbout();
    }



    private void openAbout() {
        Platform.runLater(() -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("about/about.fxml"));
                Stage stage = new Stage(StageStyle.UTILITY);
                stage.setTitle("Willkommen bei BaVeL");
                stage.setAlwaysOnTop(true);
                stage.setResizable(false);
                stage.setScene(new Scene(root));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    private class CountMemberListCell extends ListCell<CountMember> {
        private final Label name ;
        private final Label position ;
        private final GridPane pane ;
        private final CheckBox actionBtn = new CheckBox("");
        private final AtomicReference<CountMember> member = new AtomicReference<>(null);

        public CountMemberListCell() {
            super();
            name = new Label();
            position = new Label();
            position.setPrefWidth(15);
            pane = new GridPane();
            pane.add(position, 0, 0);
            pane.add(name, 1, 0);
            pane.add(actionBtn, 2, 0);
            setText(null);
            ColumnConstraints pCol = new ColumnConstraints(15);
            pCol.setHgrow(Priority.SOMETIMES);
            ColumnConstraints nCol = new ColumnConstraints();
            nCol.setHgrow(Priority.ALWAYS);
            ColumnConstraints aCol = new ColumnConstraints(30);
            aCol.setHgrow(Priority.NEVER);
            pane.getColumnConstraints().addAll(
                    pCol, nCol, aCol
            );
        }

        @Override
        protected void updateItem(CountMember item, boolean empty) {
            super.updateItem(item, empty);
            if (item==null || empty) {
                name.setText("");
                if (member.get()!=null) {
                    actionBtn.selectedProperty().unbindBidirectional(member.get().selectedProperty());
                }
                member.set(null);
                position.setText("");
                setGraphic(null);
            } else {
                name.setText(item.toString());
                member.set(item);
                int i = countMembers.indexOf(item);
                position.setText(String.valueOf(i+1));
                actionBtn.selectedProperty().bindBidirectional(member.get().selectedProperty());
                setGraphic(pane);
            }
        }
    }
}
