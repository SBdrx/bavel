package bavel.model;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Store {
    public static final byte START = (byte)0x28; // (
    public static final byte END = (byte)0x29;   // )
    public static final byte ESC = (byte)0x5C;   // \

    private final List<Topic> topics;
    private final List<Member> members;

    public Store(List<Topic> topics, List<Member> members) {
        this.topics = topics;
        this.members = members;
    }

    static ByteArrayInputStream readToByte(InputStream in) throws IOException {
        ByteArrayOutputStream outputStream = read(in);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    static String readToString(InputStream in) throws IOException {
        ByteArrayOutputStream outputStream = read(in);
        return new String(outputStream.toByteArray(), Charset.forName("UTF-8"));
    }

    private static ByteArrayOutputStream read(InputStream in) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] c = new byte[1];
        int len;
        boolean esc = false;
        int level = 0;
        int p = 0;
        while ( (len = in.read(c))!=-1) {
            if (p>0 && c[0] == ESC) {
                esc = true;
                outputStream.write(c);
            } else if (esc) {
                esc = false;
                outputStream.write(c);
            } else if (c[0] == START) {
                if (level>0) {
                    outputStream.write(c);
                }
                level++;
            } else if (p>0 && c[0] == END) {
                level--;
                if (level<=0) {
                    break;
                } else {
                    outputStream.write(c);
                }
            } else if (p==0) {
                throw new IOException("Unhandeld mode at Position " + p + " with Char '" + new String(c) + "'");
            } else {
                outputStream.write(c);
            }
            p++;
        }
        System.out.println("Read Field: " + new String(outputStream.toByteArray()));
        return outputStream;
    }

    static void writeStartField(OutputStream out) throws IOException {
        out.write(START);
    }

    static void writeEndField(OutputStream out) throws IOException {
        out.write(END);
    }

    static void writeFieldValue(String field, OutputStream out) throws IOException {
        field = field.replaceAll("\\(", "\\("); // Looks strange but makes every ( a \(
        field = field.replaceAll("\\)", "\\)");
        out.write(field.getBytes(Charset.forName("UTF-8")));
    }

    static void writeField(String field, OutputStream out) throws IOException {
        writeStartField(out);
        writeFieldValue(field, out);
        writeEndField(out);
    }


    public static Store deserialize(InputStream in) throws IOException {
        ArrayList<Topic> topics = new ArrayList<>();
        ArrayList<Member> members = new ArrayList<>();

        {
            ByteArrayInputStream topicStream = readToByte(in);
            while (topicStream.available() > 0) {
                topics.add(Topic.deserialize(readToByte(topicStream)));
            }
        }

        {
            ByteArrayInputStream memberStream = readToByte(in);
            while (memberStream.available() > 0) {
                members.add(Member.deserialize(readToByte(memberStream)));
            }
        }
        return new Store(topics, members);
    }

    public void serializeTo(OutputStream out) throws IOException {
        // Start with the LIST:
        writeStartField(out);
        for (Topic topic : topics) {
            Store.writeStartField(out);
            topic.serializeTo(out);
            writeEndField(out);
        }
        writeEndField(out);

        writeStartField(out);
        for (Member member : members) {
            Store.writeStartField(out);
            member.serializeTo(out);
            Store.writeEndField(out);
        }
        writeEndField(out);
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public List<Member> getMembers() {
        return members;
    }
}
