package bavel.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class CountMember extends Member {
    private final AtomicInteger count = new AtomicInteger(0);
    private final BooleanProperty selected = new SimpleBooleanProperty(false);

    static CountMember deserialize(InputStream in) throws IOException {
        Member member = Member.deserialize(in);
        CountMember countMember = new CountMember(member);
        countMember.count.set(Integer.parseInt(Store.readToString(in)));
        return countMember;
    }

    @Override
    public void serializeTo(OutputStream out) throws IOException {
        super.serializeTo(out);
        Store.writeField(String.valueOf(count.get()), out);
    }

    public CountMember(Member member) {
        super(member.getName(), member.getPreName());
    }

    public Integer getCount() {
        return count.get();
    }

    public Integer increaseCount() {
        return count.incrementAndGet();
    }

    public void reset() {
        count.set(0);
    }

    public boolean isSelected() {
        return selected.get();
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
}
