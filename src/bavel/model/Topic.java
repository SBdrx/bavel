package bavel.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Topic {
    private final IntegerProperty top = new SimpleIntegerProperty(1);
    private final StringProperty thema = new SimpleStringProperty("");
    private final IntegerProperty mode = new SimpleIntegerProperty(0);
    private final IntegerProperty counts = new SimpleIntegerProperty(0);
    private final IntegerProperty abstentions = new SimpleIntegerProperty(0);
    private final StringProperty content = new SimpleStringProperty("");
    private final ObservableList<CountMember> members = FXCollections.observableArrayList();
    private final BooleanProperty blockWahl = new SimpleBooleanProperty(false);

    public Topic() {
    }

    public Topic(Integer top, String thema) {
        this.top.setValue(top);
        this.thema.setValue(thema);
    }

    @Override
    public String toString() {
        return  top.getValue() + "\t" + thema.getValue();
    }

    static Topic deserialize(InputStream in) throws IOException {
        Topic topic = new Topic();
        topic.setTop(Integer.parseInt(Store.readToString(in)));
        topic.setThema(Store.readToString(in));
        topic.setMode(Integer.parseInt(Store.readToString(in)));
        topic.setCounts(Integer.parseInt(Store.readToString(in)));
        topic.setAbstentions(Integer.parseInt(Store.readToString(in)));
        topic.setContent(Store.readToString(in));
        topic.setBlockWahl(Store.readToString(in).equalsIgnoreCase("true"));

        ByteArrayInputStream listStream = Store.readToByte(in);
        while (listStream.available()>0) {
            topic.getMembers().add(CountMember.deserialize(Store.readToByte(listStream)));
        }

        return topic;
    }

    public void serializeTo(OutputStream out) throws IOException {
        Store.writeField(String.valueOf(top.get()), out);
        Store.writeField(thema.get(), out);
        Store.writeField(String.valueOf(mode.get()), out);
        Store.writeField(String.valueOf(counts.get()), out);
        Store.writeField(String.valueOf(abstentions.get()), out);
        Store.writeField(content.get(), out);
        Store.writeField(blockWahl.get()?"true":"false", out);

        // Start List "Members":
        Store.writeStartField(out);
        for (Member member : members) {
            Store.writeStartField(out);
            member.serializeTo(out);
            Store.writeEndField(out);
        }
        Store.writeEndField(out);
    }

    public int getTop() {
        return top.get();
    }

    public IntegerProperty topProperty() {
        return top;
    }

    public void setTop(int top) {
        this.top.set(top);
    }

    public String getThema() {
        return thema.get();
    }

    public StringProperty themaProperty() {
        return thema;
    }

    public void setThema(String thema) {
        this.thema.set(thema);
    }

    public int getMode() {
        return mode.get();
    }

    public IntegerProperty modeProperty() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode.set(mode);
    }

    public String getContent() {
        return content.get();
    }

    public StringProperty contentProperty() {
        return content;
    }

    public void setContent(String content) {
        this.content.set(content);
    }

    public ObservableList<CountMember> getMembers() {
        return members;
    }

    public boolean isBlockWahl() {
        return blockWahl.get();
    }

    public BooleanProperty blockWahlProperty() {
        return blockWahl;
    }

    public void setBlockWahl(boolean blockWahl) {
        this.blockWahl.set(blockWahl);
    }

    public int getCounts() {
        return counts.get();
    }

    public IntegerProperty countsProperty() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts.set(counts);
    }

    public int getAbstentions() {
        return abstentions.get();
    }

    public IntegerProperty abstentionsProperty() {
        return abstentions;
    }

    public void setAbstentions(int abstentions) {
        this.abstentions.set(abstentions);
    }
}
