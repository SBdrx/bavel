package bavel.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

public class Member {
    private final String name;
    private final String preName;

    public Member(String name, String preName) {
        this.name = name;
        this.preName = preName;
    }

    static Member deserialize(InputStream in) throws IOException {
        return new Member(
                Store.readToString(in),
                Store.readToString(in)
        );
    }

    public void serializeTo(OutputStream out) throws IOException {
        Store.writeField(name, out);
        Store.writeField(preName, out);
    }


    public String toString() {
        return (preName!=null && !preName.isEmpty()?preName+ " ":"") + name;
    }

    public String getName() {
        return name;
    }

    public String getPreName() {
        return preName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return Objects.equals(name, member.name) &&
                Objects.equals(preName, member.preName);
    }

    public boolean isEqualTo(String str) {
            return str.equalsIgnoreCase(toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, preName);
    }
}
