package bavel.about;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;

public class About {

    @FXML
    void onBtnOk(ActionEvent event) {
        ((Stage)((Node)event.getTarget()).getScene().getWindow()).close();
    }

}
