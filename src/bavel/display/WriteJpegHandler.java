package bavel.display;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class WriteJpegHandler implements DrawEvent.Handler, AutoCloseable {
    private final File directory;
    private AtomicLong frame = new AtomicLong(0);
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public WriteJpegHandler(File directory) {
        if (!directory.exists() && !directory.mkdirs()) throw new RuntimeException("Could not Create Directory " + directory.getAbsolutePath());
        this.directory = directory;
    }

    @Override
    public void handleDraw(DrawEvent event) {
        WritableImage snapshot = event.getPane().snapshot(new SnapshotParameters(), null);
        File image = new File(directory, "frame_" + frame.getAndIncrement() + ".png");
        executor.execute(() -> {
            try {
                BufferedImage bufferedImage = SwingFXUtils.fromFXImage(snapshot, null);
                ImageIO.write(bufferedImage, "png", image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void close() throws Exception {
        executor.shutdownNow();
    }
}
