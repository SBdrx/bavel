package bavel.display;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class DisplayPaneSettings {
    private float headerPercent = 0.08f;
    private float footerPercent = 0.08f;
    private float padding = 0.02f;
    private final double width;
    private final double height;

    public DisplayPaneSettings(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public float getHeaderPercent() {
        return headerPercent;
    }

    public float getFooterPercent() {
        return footerPercent;
    }

    public void setHeaderPercent(float headerPercent) {
        this.headerPercent = headerPercent;
    }

    public void setFooterPercent(float footerPercent) {
        this.footerPercent = footerPercent;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double padding() {
        return Math.min(width*padding, height*padding);
    }

    public float getPadding() {
        return padding;
    }

    public void setPadding(float padding) {
        this.padding = padding;
    }

    public String fontName() {
        return "Calibri";
    }

    public Paint colorGrey() {
        return Color.valueOf("#fefefe");
    }

    public Color mainColor() {
        return Color.valueOf("#009ee3");
    }

    public Color mainBgColor() {
        return Color.valueOf("ffed00");
    }

    public Color yellowColor() {
        return Color.valueOf("#ffed00");
    }

    public Color magentaColor() {
        return Color.valueOf("#e5007d");
    }
}
