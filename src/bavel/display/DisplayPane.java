package bavel.display;

import bavel.model.Member;
import bavel.model.Topic;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;

import static java.time.temporal.ChronoField.*;

public class DisplayPane implements AutoCloseable {
    private static final DateTimeFormatter ISO_LOCAL_TIME = new DateTimeFormatterBuilder()
                .appendValue(HOUR_OF_DAY, 2)
                .appendLiteral(':')
                .appendValue(MINUTE_OF_HOUR, 2)
                .optionalStart()
                .appendLiteral(':')
                .appendValue(SECOND_OF_MINUTE, 2)
            .toFormatter();
    private final double[] scale;
    private Pane pane;
    private DisplayPaneSettings settings;
    private final ObjectProperty<Topic> topicProperty = new SimpleObjectProperty<>(null);
    private final IntegerProperty top = new SimpleIntegerProperty(1);
    private final StringProperty thema = new SimpleStringProperty("");
    private final IntegerProperty mode = new SimpleIntegerProperty(0);
    private final StringProperty content = new SimpleStringProperty("");
    private final ObjectProperty<DrawEvent.Handler> drawEventEventHandler = new SimpleObjectProperty<>();
    private ListChangeListener<Member> memberListener = c -> Platform.runLater(this::updatePane);


    public DisplayPane(Pane pane, DisplayPaneSettings settings) {
        this.pane = pane;
        this.settings = settings;
        pane.setMaxSize(settings.getWidth(), settings.getHeight());
        pane.setPrefSize(settings.getWidth(), settings.getHeight());
        pane.setMinSize(settings.getWidth(), settings.getHeight());

        {
            Text text = new Text("Dies ist ein Beispiel!");
            Font font = fontBold(10);
            text.setFont(font);
            text.applyCss();
            scale = new double[3];
            scale[0] = 10 / text.getLayoutBounds().getHeight();
            text.setFont(fontNorm(10));
            text.applyCss();
            scale[1] = 10 / text.getLayoutBounds().getHeight();
            text.setFont(fontMono(10));
            text.applyCss();
            scale[2] = 10 / text.getLayoutBounds().getHeight();
        }
        topicProperty.addListener((observable, oldValue, newValue) -> {
            if (oldValue != null) {
                unbind(oldValue);
            }
            if (newValue != null) {
                bind(newValue);
            }
        });

        top.addListener((observable, oldValue, newValue) -> this.updatePane());
        mode.addListener((observable, oldValue, newValue) -> this.updatePane());
        content.addListener((observable, oldValue, newValue) -> this.updatePane());
        thema.addListener((observable, oldValue, newValue) -> this.updatePane());
        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (pane.getChildren().size()>0) {
                    Node node = pane.getChildren().get(0);
                    if (node instanceof Text) {
                        ((Text) node).setText(LocalTime.now().format(ISO_LOCAL_TIME));
                    }
                }
            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }

    public void updatePane() {
        pane.getChildren().clear();
        double padding = settings.padding();

        {
            // Add Time
            Text time = new Text(LocalTime.now().format(ISO_LOCAL_TIME));
            time.setFill(settings.colorGrey());
            final double footerHeight = (settings.getHeight() * settings.getFooterPercent()) - 2 * padding;
            double footerFontSize = scale[1] * footerHeight;
            time.setFont(fontNorm(footerFontSize));
            time.applyCss();
            while (time.getLayoutBounds().getWidth() > (settings.getWidth()/2) - 20) {
                footerFontSize -= 0.2;
                time.setFont(fontNorm(footerFontSize));
                time.applyCss();
            }
            time.setLayoutX(settings.getWidth() - padding - (time.getLayoutBounds().getWidth()));
            time.setLayoutY(settings.getHeight() - footerHeight + padding);
            pane.getChildren().add(time);
        }

        // Add Topic:
        {
            final double headerHeight = (settings.getHeight() * settings.getHeaderPercent()) - 2 * padding;
            Text topicTitle = addText(
                    top.getValue() + ". " + thema.getValue(),
                    scale[0]* headerHeight,
                     settings.padding(),
                    settings.padding() + headerHeight,
                    settings.getWidth() - 2*padding,
                    0,
                    3, settings.mainColor(), settings.mainBgColor(), false);

        }



        if (mode.getValue()==0) {
            // Text Mode:
            addTextMode();
        } else if (mode.getValue()==1 || mode.getValue()==2) {
            addWahlMembers();
        }

        if (drawEventEventHandler.get()!=null) {
            drawEventEventHandler.get().handleDraw(new DrawEvent(pane));
        }
    }

    private void addTextMode() {
        if (content.getValue()==null || content.getValue().isEmpty()) return;
        String[] lines = content.getValue().split("[\n\r]");
        int odd = 2;
        int even = 6;

        writeLines(lines, odd, even, settings.yellowColor(), settings.magentaColor(), false);
    }



    private void addWahlMembers() {
        String[] names;
        int ml = 25;
        boolean countMode = (mode.getValue() == 2);
        if (topicProperty.get()==null || topicProperty.get().getMembers().size()==0) {
            names = new String[]{"1. ..."};
        } else {
            List<? extends Member> members;
            members = topicProperty.get().getMembers();
            names = new String[members.size() + (countMode ?3:0)];
            for (int i = 0; i < members.size(); i++) {
                names[i] = (i+1) + "." + (countMode?(members.size()>9&&i<9?" ":"")+" ":"\t")  + members.get(i).toString().trim();
                if (names[i].length()>ml) {
                    ml = names[i].length();
                }
            }
            if (countMode) {
                for (int i = 0; i < members.size(); i++) {
                    while (names[i].length() < ml) {
                        names[i] += ".";
                    }
                    names[i] += "  " + topicProperty.get().getMembers().get(i).getCount();
                }
                {
                    int p = members.size();
                    String text = p>1?"Enthaltungen":"Gegenstimmen";
                    int value = topicProperty.get().getAbstentions();

                    setOptionalText(names, ml, members, p, text, value);
                    names[p+1] = "";
                    setOptionalText(names, ml, members, p+2, "Stimmen", topicProperty.get().getCounts());
                }

            }
        }

        writeLines(names, 2, 2, settings.yellowColor(), null, countMode);
    }

    private void setOptionalText(String[] names, int ml, List<? extends Member> members, int p, String text, int value) {
        names[p] = (members.size() > 9 ? "    " : "   ") + text;
        while (names[p].length() < ml) {
            names[p] += ".";
        }
        names[p] += "  " + value;
    }

    private double writeLines(String[] lines, int odd, int even, Color fill, Color bg, boolean mono) {
        if (lines.length==0) return 0;
        String longesLine = null;
        for (String line : lines) {
            if (longesLine==null || line.length()>longesLine.length()) longesLine = line;
        }
        double fs = fontSizeToMatch(settings.getWidth() - (2 * Math.max(odd, even)*settings.padding()), mono?2:0, longesLine);
        double lineHeight = fs / scale[mono?2:0];
        double height = settings.getHeight() * ( 1 - settings.getHeaderPercent() - settings.getFooterPercent()) - 8*settings.padding();
        while (lineHeight * lines.length > height) {
            fs -= 0.1;
            lineHeight = fs / scale[mono?2:0];
        }
        double deltaY = (height - (lineHeight * lines.length)) / 2;
        double y = (settings.getHeight() * settings.getHeaderPercent()) + 4*settings.padding() + deltaY;

       /* Rectangle r = new Rectangle(settings.getWidth()-2* settings.padding(), lineHeight * lines.length);
        r.setX(settings.padding());
        r.setY(y);
        this.pane.getChildren().add(r);*/

        boolean p = true;
        for (String line : lines) {

            Text node = addText(line, fs, settings.padding()*(p? odd : even), y, settings.getWidth() - 2*settings.padding(), 0, 0, fill, bg, mono);
            y += node.getLayoutBounds().getHeight();
            p = !p;
        }
        return fs;
    }

    private Font font(int mode, double fs) {
        switch (mode) {
            case 2: return fontMono(fs);
            case 1: return fontNorm(fs);
            case 0:
            default:
                return fontBold(fs);
        }
    }

    private double fontSizeToMatch(double maxSize, int mode , String longesLine) {
        double fs = maxSize * scale[mode];
        Text text = new Text(longesLine);
        text.setFont(font(mode, fs));
        text.applyCss();
        while (text.getLayoutBounds().getWidth() <= maxSize) {
            fs += 0.1;
            text.setFont(font(mode, fs));
            text.applyCss();
        }
        while (text.getLayoutBounds().getWidth() >= maxSize) {
            fs -= 0.1;
            text.setFont(font(mode, fs));
            text.applyCss();
        }
        return fs;
    }

    private Text addText(String textToShow, double headerFontSize, double x, double y, double maxWidth, double maxHeight, int mode, Color fill, Color bg, boolean mono) {
        Text text = new Text(textToShow);
        text.setFont(mono?fontMono(headerFontSize):fontBold(headerFontSize));
        text.applyCss();
        while (
                (maxWidth > 0 && text.getLayoutBounds().getWidth() > maxWidth)
                || (maxHeight > 0 && text.getLayoutBounds().getHeight() > maxHeight)
        ) {
            headerFontSize -= 0.1;
            text.setFont(mono?fontMono(headerFontSize):fontBold(headerFontSize));
            text.applyCss();
        }
        if (mode == 0) {
            text.setLayoutX(x);
            text.setLayoutY(y + text.getLayoutBounds().getHeight() - text.getLayoutBounds().getMaxY());
        } else if (mode==1) {
            text.setLayoutX(x - text.getLayoutBounds().getWidth());
            text.setLayoutY(y + text.getLayoutBounds().getMinY());
        } else if (mode==2) {
            text.setLayoutX(x - text.getLayoutBounds().getWidth());
            text.setLayoutY(y);
        } else if (mode==3) {
            text.setLayoutX(x);
            text.setLayoutY(y);
        }

        if (fill!=null) {
            text.setFill(fill);
            text.applyCss();
        }
        if (bg!=null && !textToShow.trim().isEmpty()) {
            double pv = settings.padding() / 2.0f;
            Rectangle rectangle = new Rectangle(text.getLayoutBounds().getWidth() + settings.padding(),
                    text.getLayoutBounds().getHeight() + settings.padding(), bg);
            rectangle.setX(text.getLayoutX() - pv);
            rectangle.setY(text.getLayoutY() + text.getLayoutBounds().getMinY() - pv /* - text.getLayoutBounds().getHeight()+pv*/ );
            pane.getChildren().add(rectangle);
        }
        pane.getChildren().add(text);
        return text;
    }

    private Font fontMono(double size) {
        return Font.font("Courier New", FontWeight.BOLD, size);
    }

    private Font fontBold(double size) {
        return Font.font(settings.fontName(), FontWeight.BOLD, size);
    }

    private Font fontNorm(double size) {
        return Font.font(settings.fontName(), FontWeight.NORMAL, size);
    }


    private void bind(Topic newValue) {
        top.bind(newValue.topProperty());
        mode.bind(newValue.modeProperty());
        content.bind(newValue.contentProperty());
        thema.bind(newValue.themaProperty());
        newValue.getMembers().addListener(memberListener);
    }

    private void unbind(Topic oldValue) {
        top.unbind();
        mode.unbind();
        content.unbind();
        thema.unbind();
        oldValue.getMembers().removeListener(memberListener);
    }

    public Topic getTopic() {
        return topicProperty.get();
    }

    public ObjectProperty<Topic> topicProperty() {
        return topicProperty;
    }

    public void setTopic(Topic topicProperty) {
        this.topicProperty.set(topicProperty);
    }

    @Override
    public void close() throws Exception {
        pane = null;
    }

    public DrawEvent.Handler getDrawEventEventHandler() {
        return drawEventEventHandler.get();
    }

    public void setDrawEventEventHandler(DrawEvent.Handler drawEventEventHandler) {
        this.drawEventEventHandler.set(drawEventEventHandler);
    }
}
