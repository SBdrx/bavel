package bavel.display;

import javafx.scene.layout.Pane;

public class DrawEvent  {
    public interface Handler {
        void handleDraw(DrawEvent event);
    }
    private final Pane pane;

    public DrawEvent(Pane pane) {
        this.pane = pane;
    }

    public Pane getPane() {
        return pane;
    }
}
