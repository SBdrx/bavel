package bavel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.InputStream;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("BaVeL - Basis Veranstaltungs Leitsystem");
        InputStream resourceAsStream = Main.class.getResourceAsStream("bavel-b-logo.png");
        if (resourceAsStream!=null) {
            primaryStage.getIcons().add(new Image(resourceAsStream));
        }
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        Controller controller = loader.getController();
        primaryStage.setOnCloseRequest(event -> {
            controller.close(); // Will Exit and Close all
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
